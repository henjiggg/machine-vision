﻿using MachineVision.Core;
using MachineVision.Services;
using Prism.Regions; 

namespace MachineVision.ViewModels
{
    public class DashboardViewModel : NavigationViewModel
    {
        public DashboardViewModel(INavigationMenuService menuService)
        {
            MenuService = menuService;
        }

        public INavigationMenuService MenuService { get; }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        { 
            base.OnNavigatedTo(navigationContext);
        }
    }
}
