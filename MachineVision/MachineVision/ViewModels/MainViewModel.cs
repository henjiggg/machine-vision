﻿using MachineVision.Core;
using MachineVision.Models;
using MachineVision.Services;
using Prism.Commands;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MachineVision.ViewModels
{
    public class MainViewModel : NavigationViewModel
    {
        public MainViewModel(IRegionManager region, INavigationMenuService menuService)
        {
            this.region = region;
            MenuService = menuService;
            NavigateCommand = new DelegateCommand<NavigationItem>(Navigate);
        }

        public INavigationMenuService MenuService { get; }

        public DelegateCommand<NavigationItem> NavigateCommand { get; private set; }

        private int selectedIndex = -1;

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set { selectedIndex = value; RaisePropertyChanged(); }
        }

        private bool isTopDrawerOpen;
        private readonly IRegionManager region;

        public bool IsTopDrawerOpen
        {
            get { return isTopDrawerOpen; }
            set
            {
                isTopDrawerOpen = value;
                if (SelectedIndex == 0 && !value)
                    SelectedIndex = -1;
                RaisePropertyChanged();
            }
        }

        private void Navigate(NavigationItem item)
        {
            if (item == null) return;

            if (item.Name.Equals("全部"))
            {
                IsTopDrawerOpen = true;
                return;
            }

            IsTopDrawerOpen = false;
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            MenuService.Initialize();
            region.Regions["MainViewContentRegion"].RequestNavigate("DashboardView", back =>
            {
                if(!(bool)back.Result)
                {

                }
            });
            base.OnNavigatedTo(navigationContext);
        }
    }
}
 