﻿using MachineVision.Models;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineVision.Services
{
    public class NavigationMenuService : BindableBase, INavigationMenuService
    {
        public NavigationMenuService()
        {
            MenuItems = new ObservableCollection<NavigationItem>();
        }

        private ObservableCollection<NavigationItem> menuItems;

        public ObservableCollection<NavigationItem> MenuItems
        {
            get { return menuItems; }
            set { menuItems = value; RaisePropertyChanged(); }
        }

        public void Initialize()
        {
            MenuItems.Clear();
            MenuItems.Add(new NavigationItem("", "全部", "", new ObservableCollection<NavigationItem>()
            {
                 new NavigationItem("","模板匹配","",new ObservableCollection<NavigationItem>()
                 {
                      new NavigationItem("ShapeCirclePlus","轮廓匹配",""),
                      new NavigationItem("ShapeOutline","形状匹配",""),
                      new NavigationItem("Clouds", "相似性匹配",""),
                      new NavigationItem("ShapeOvalPlus","形变匹配",""),
                 }),
                 new NavigationItem("", "比较测量","",new ObservableCollection<NavigationItem>()
                 {
                      new NavigationItem("Circle","卡尺找圆",""),
                      new NavigationItem("Palette","颜色检测",""),
                      new NavigationItem("Ruler", "几何测量",""),
                 }),
                 new NavigationItem("","字符识别","",new ObservableCollection<NavigationItem>()
                 {
                      new NavigationItem("FormatColorText", "字符识别",""),
                      new NavigationItem("Barcode", "一维码识别",""),
                      new NavigationItem("Qrcode", "二维码识别",""),
                 }),
                 new NavigationItem("","缺陷检测","",new ObservableCollection<NavigationItem>()
                 {
                      new NavigationItem("Crop", "差分模型",""),
                      new NavigationItem("CropRotate", "形变模型",""),
                 })
            }));
            MenuItems.Add(new NavigationItem("", "模板匹配", ""));
            MenuItems.Add(new NavigationItem("", "比较测量", ""));
            MenuItems.Add(new NavigationItem("", "字符识别", ""));
            MenuItems.Add(new NavigationItem("", "缺陷检测", ""));
            MenuItems.Add(new NavigationItem("", "学习文档", ""));
        }
    }
}
