﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineVision.Models
{
    /// <summary>
    /// 菜单模型
    /// </summary>
    public class NavigationItem : BindableBase
    {
        public NavigationItem(
           string icon,
           string name,
           string pageName,
           ObservableCollection<NavigationItem> items = null)
        {
            Icon = icon;
            Name = name;
            PageName = pageName;
            Items = items;
        }

        private string name;
        private string icon;
        private ObservableCollection<NavigationItem> items;

        public string Icon
        {
            get { return icon; }
            set { icon = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged(); }
        }

        public string PageName { get; set; }

        /// <summary>
        /// 导航菜单列表
        /// </summary>
        public ObservableCollection<NavigationItem> Items
        {
            get { return items; }
            set { items = value; RaisePropertyChanged(); }
        }
    }
}
