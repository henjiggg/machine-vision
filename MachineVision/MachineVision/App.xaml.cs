﻿using MachineVision.Services;
using MachineVision.ViewModels;
using MachineVision.Views;
using Prism.DryIoc;
using Prism.Ioc;
using Prism.Regions;
using System.Windows;

namespace MachineVision
{
    public partial class App : PrismApplication
    {
        protected override Window CreateShell() => null;

        protected override void RegisterTypes(IContainerRegistry services)
        {
            services.RegisterSingleton<INavigationMenuService, NavigationMenuService>();
            services.RegisterForNavigation<MainView, MainViewModel>();
            services.RegisterForNavigation<DashboardView, DashboardViewModel>();
        }

        protected override void OnInitialized()
        {
            var container = ContainerLocator.Container;
            var shell = container.Resolve<object>("MainView");
            if (shell is Window view)
            {
                var regionManager = container.Resolve<IRegionManager>();
                RegionManager.SetRegionManager(view, regionManager);
                RegionManager.UpdateRegions();

                if (view.DataContext is INavigationAware navigationAware)
                {
                    navigationAware.OnNavigatedTo(null);
                    App.Current.MainWindow = view;
                }
            }
            base.OnInitialized();
        }
    }
}
